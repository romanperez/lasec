﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Threading;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Run r = new Run("Liebre", 10);
            Run p = new Run("Tortuga", 2);
            r.runing += new Run.run(notificar);
            p.runing += new Run.run(notificar);
            Thread t = new Thread(r.corriendo);
            Thread k = new Thread(p.corriendo);
            t.Start();            
            k.Start();
            r.durmiendo();
            Console.Read();
        }
        public void notificar(string estado)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() => listBox1.Items.Add(estado)));
                Invoke(new Action(()=>listBox1.SelectedIndex=listBox1.Items.Count-1));
            }
        }
    }
}
