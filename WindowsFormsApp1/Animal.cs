﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Animal
    {
        public string nombre;
        public int velocidad;
        private bool dormir;

        public Animal(string nom, int vel)
        { nombre = nom;
            velocidad = vel;
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public int Velocidad { get => velocidad; set => velocidad = value; }

        public void durmiendo()
        {
            Console.WriteLine("Se ha dormido69 " + nombre);
        }

        public void despertando()
        {
            Console.WriteLine("Se ha despertado4956918 " + nombre);
        }
    }

}
